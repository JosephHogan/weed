const express = require('express')
const axios = require('axios').default;
const { parse } = require('json2csv');
const app = express()
const port = 3000
const weedApi = `https://api-g.weedmaps.com`;
const limit = 150;
const fs = require('fs');
const path = require('path');
const newLine = '\r\n';

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});

const delay = function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const getDispensaries = (page) => {
    let data = [];
    page = page || 1;
    let headers = {
        Cookie: '',
    }
    headers['user-agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36';
    const getByPage = (page) => {
        return axios.get(`${weedApi}/discovery/v2/listings?page_size=${limit}&page=${page}`, {headers, withCredentials: true})
        .then(res => {
            if (res.data.data.listings.length) {
                data = data.concat(res.data.data.listings);
                page++;
                if(res.data.data.listings.length === limit) {
                    return getByPage(page);
                }
            }
            return data;
        }).catch(err => {
            if ([401, 403].indexOf(err.response.status) > -1) {
                headers.Cookie = err.response.headers['set-cookie'].join(',');
                console.log(`Retrying Dispensaries page ${page}...`);
                return delay(60000)
                .then(() => getByPage(page));
            }
            return data;
        })
    };
    return getByPage(page);
};

const getProducts = (dispensary, page) => {
    let data = [];
    page = page || 1;
    let headers = {
        Cookie: '',
    }
    headers['user-agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36';
    const getByPage = (page) => {
        console.log(`Getting dispensary ${dispensary} page ${page}`);
        return axios.get(`${weedApi}/discovery/v1/listings/dispensaries/${dispensary}/menu_items?page_size=${limit}&page=${page}`, {headers, withCredentials: true})
        .then(res => {
            if (res.data.data.menu_items.length) {
                data = data.concat(res.data.data.menu_items.map(item => {
                    item.dispensary = dispensary;
                    return item;
                }));
                page++;
                if(res.data.data.menu_items.length === limit) {
                    return getByPage(page);
                }
            }
            return data;
        })
        .catch(err => {
            if ([401, 403].indexOf(err.response.status) > -1) {
                headers.Cookie = err.response.headers['set-cookie'].join(',');
                console.log(`Retrying Dispensary ${dispensary} page ${page}...`);
                return delay(60000)
                .then(() => getByPage(page));
            }
            return data;
        })
    };
    return getByPage(page);
};

const writeResults = (slug, data) => {
    const manifest = require('./dump/manifest.json') || {};
    manifest[slug] = data ? data.length : 0;
    fs.writeFileSync(path.resolve(__dirname, 'dump/manifest.json'), JSON.stringify(manifest));
    if (data && data.length) {
        const csv = newLine + parse(data, {header: false});
        fs.appendFileSync(path.resolve(__dirname, `dump/products.csv`), csv);
    }
};

const getProductsSync = (slugs) => {
    let i = 0;
    const get = () => {
        return getProducts(slugs[i])
        .then((res) => {
            console.log(`writing results for item ${i+1} of ${slugs.length}: ${slugs[i]}`);
            writeResults(slugs[i], res);
            if(i < slugs.length) {
                i++;
                return get();
            }
            return;
        })
    }
    return get();
}

getDispensaries()
.then(data => {
    const csv = parse(data);
    fs.writeFileSync(path.resolve(__dirname, 'dump/dispensaries.csv'), csv);
    return data;
})
.then((data) => {
    const manifest = require('./dump/manifest.json') || {};
    getProductsSync(data.map(item => item.slug).filter(slug => !!slug).filter(slug => typeof manifest[slug] === 'undefined'))
    .then(() => {
        console.log('DONE!');
    })
})
